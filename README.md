# try-dequeue

## Example Code
Simple example of using *Dequeue*

```haskell
module Main where

-- Docs: http://hackage.haskell.org/package/dequeue-0.1.12/docs/Data-Dequeue.html
import Data.Dequeue as D

main :: IO ()
main = do

  let a = (D.empty :: D.BankersDequeue Char)
  let a' = D.pushFront a 'a'
  let b = D.first a'
  print b
  ```

## Run Code
```shell
git clone git@github.com:BebeSparkelSparkel/try-dequeue.git
cd try-dequeue
stack build
stack repl
```

In repl run *main*
```shell
*Main> main
Just 'a'
*Main>
```

